import { useEffect, useState } from "react";
import axios from "axios";

const useFetch = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const result = await axios.get(`https://backend-healing.herokuapp.com/hotels`);
                setData(result.data.data.map(item => {
                    return {
                        id: item.id,
                        title: item.title,
                        image: item.image,
                        city: item.city,
                        addres: item.address,
                        desc: item.desc,
                        rating: item.rating,
                        featured: item.featured,
                        starttingPrice: item.starttingPrice
                    }
                }));
            } catch (err) {
                setError(err);
            }
            setLoading(false);
        };
        fetchData();
    }, []);

    const reFetch = async () => {
        setLoading(true);
        try {
            const result = await axios.get(`https://backend-healing.herokuapp.com/hotels`);
            setData(result.data.data.map(item => {
                return {
                    id: item.id,
                    title: item.title,
                    image: item.image,
                    city: item.city,
                    addres: item.address,
                    desc: item.desc,
                    rating: item.rating,
                    featured: item.featured,
                    starttingPrice: item.starttingPrice
                }
            }));
        } catch (err) {
            setError(err);
        }
        setLoading(false);
    };

    return { data, loading, error, reFetch };
};

export default useFetch;