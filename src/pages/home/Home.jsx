import React from 'react'
import "./home.css"
import Header from '../../components/header/Header'
import Navbar from '../../components/navbar/Navbar'
import Featured from '../../components/featured/Featured'
import PropertyList from '../../components/propertyList/PropertyList'
import FeaturedProperties from '../../components/FeaturedProperties/FeaturedProperties'
import Footer from '../../components/footer/Footer'
import MailList from '../../components/mailList/MailList'

const Home = () => {
  return (
    <div>
      <Navbar />
      <Header />
      <div className="homeContainer">
        <Featured />
        <h1 className="homeTitle">Healing Service Guarantee</h1>
        <PropertyList />
        <h1 className="homeTitle">Most Popular Hotels</h1>
        <FeaturedProperties />
        <MailList />
        <Footer />
      </div>
    </div>

  )
}

export default Home