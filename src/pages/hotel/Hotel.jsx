import Header from "../../components/header/Header"
import Navbar from "../../components/navbar/Navbar"
import "./hotel.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleArrowLeft, faCircleArrowRight, faCircleXmark, faLocationDot } from "@fortawesome/free-solid-svg-icons";
import Footer from "../../components/footer/Footer";
import MailList from "../../components/mailList/MailList";
import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";



const Hotel = () => {
  const { id } = useParams();
  const [hotel, setHotel] = useState({})
  const [photos, setPhotos] = useState({})

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`https://backend-healing.herokuapp.com/hotels/${id}`)
      setHotel(result.data.data)
    }
    fetchData()
  }, [])
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`https://backend-healing.herokuapp.com/hotels/${id}`)
      setPhotos(result.data.data.photos)
      console.log(photos)
    }
    fetchData()
  }, [])

  const [slideNumber, setSlideNumber] = useState(0)
  const [open, setOpen] = useState(false)

  const handleOpen = (i) => {
    setSlideNumber(i)
    setOpen(true)
  }

  const handleMove = (direction) => {
    let newSlideNumber
    if (direction === "left") {
      newSlideNumber = slideNumber === 0 ? 5 : slideNumber - 1
    } else {
      newSlideNumber = slideNumber === 5 ? 0 : slideNumber + 1
    }

    setSlideNumber(newSlideNumber)
  }


  return (
    <div>
      <Navbar />
      <Header type="list" />
      <div className="hotelContainer">
        {<div className="hotelWrapper">
          <button className="bookNow">Reserve or Book Now!</button>
          <h1 className="hotelTitle">{hotel.title}</h1>
          <div className="hotelAddress">
            <FontAwesomeIcon icon={(faLocationDot)} />
            <span>{hotel.addres}</span>
          </div>
          <span className="hotelDistance">
            Excellent location – 500m from center
          </span>
          <span className="hotelPriceHighlight">
            Book a stay over $10 at this property and get a
            free airport taxi
          </span>
          <div className="hotelImages">
            <div className="hotelImgWrapper" >
              <img src={photos.imageUrl1} className="hotelImg" />
              <img src={photos.imageUrl2} className="hotelImg" />
              <img src={photos.imageUrl3} className="hotelImg" />
              <img src={photos.imageUrl4} className="hotelImg" />
              <img src={photos.imageUrl5} className="hotelImg" />
              <img src={photos.imageUrl6} className="hotelImg" />
            </div>
          </div>
          <div className="hotelDetails">
            <div className="hotelDetailsTexts">
              <h1 className="hotelTitle">{hotel.title}</h1>
              <p className="hotelDesc">{hotel.desc}</p>
            </div>
            <div className="hotelDetailsPrice">
              <h1>Perfect for a 1-night stay!</h1>
              <span>
                {hotel.featured}
              </span>
              <h2>
                <b>${hotel.starttingPrice}</b>(1 night)
              </h2>
              <button >Reserve or Book Now!</button>
            </div>
          </div>
        </div>}
        <MailList />
        <Footer />
      </div>
    </div>
  )
}

export default Hotel