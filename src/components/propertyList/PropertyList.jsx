import { faBed, faBottleWater, faShower, faTelevision, faWifi } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import "./propertyList.css"

const PropertyList = () => {
    return (
        <div className="pList">
            <div className="pListItem">
                <FontAwesomeIcon icon={(faWifi)} style={{ fontSize: "80px", color: "red", alignContent: "center" }} />
                <div className="pListTitles">
                    <h1>Free Wifi</h1>
                </div>
            </div>
            <div className="pListItem">
                <FontAwesomeIcon icon={(faTelevision)} style={{ fontSize: "80px", color: "red", alignContent: "center" }} />
                <div className="pListTitles">
                    <h1>Satellite Television</h1>
                </div>
            </div>
            <div className="pListItem">
                <FontAwesomeIcon icon={(faBottleWater)} style={{ fontSize: "80px", color: "red", margin: "auto" }} />
                <div className="pListTitles">
                    <h1>Mineral Water</h1>
                </div>
            </div>

            <div className="pListItem">
                <FontAwesomeIcon icon={(faBed)} style={{ fontSize: "80px", color: "red", alignContent: "center" }} />
                <div className="pListTitles">
                    <h1>Spotless Linen</h1>
                </div>
            </div>

            <div className="pListItem">
                <FontAwesomeIcon icon={(faShower)} style={{ fontSize: "80px", color: "red", alignContent: "center" }} />
                <div className="pListTitles">
                    <h1>Clean Washrooms</h1>
                </div>
            </div>
        </div>
    )
}

export default PropertyList