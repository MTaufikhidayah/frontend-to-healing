import React from 'react'
import { Link } from "react-router-dom";
import "./navbar.css"

const Navbar = () => {
  return (
    <div className="navBar">
      <div className="navContainer">
        <Link to={`/`} style={{ textDecoration: 'none', color: "white" }}>
          <span className="logo">Healing.com</span>
        </Link>
        <div className="navItems">
          <button div className="navButton">Register</button >
          <button className="navButton">Login</button >
        </div>
      </div>
    </div>
  )
}

export default Navbar