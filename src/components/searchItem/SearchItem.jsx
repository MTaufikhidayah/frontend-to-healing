import "./searchItem.css"
import { Link } from "react-router-dom";

const SearchItem = ({ data }) => {
    return (
        <>
            {data.map((hotel) => (<div className="searchItem">
                <img src={hotel.image}
                    alt=""
                    className="siImg"
                />
                <div className="siDesc">
                    <h1 className="siTitle">{hotel.title}</h1>
                    <span className="siDistance">{hotel.city}</span>
                    <span className="siTaxiOp">Free airport taxi</span>
                    <span className="siSubtitle">
                        Studio Apartment with Air conditioning
                    </span>
                    <span className="siFeatures">{hotel.featured}</span>
                    <span className="siCancelOp">Free cancellation </span>
                    <span className="siCancelOpSubtitle">
                        You can cancel later, so lock in this great price today!
                    </span>
                </div>
                <div className="siDetails">
                    <div className="siRating">
                        <span>Excellent</span>
                        <button>{hotel.rating}</button>
                    </div>
                    <div className="siDetailTexts">
                        <span className="siPrice">StartingPrice From ${hotel.starttingPrice}</span>
                        <span className="siTaxOp">Includes taxes and fees</span>
                        <Link to={`/hotels/${hotel.id}`}>
                            <button className="siCheckButton">See availability</button>
                        </Link>
                    </div>
                </div>
            </div>))}







        </>
    )
}

export default SearchItem