import React from 'react'
import "./featuredProperties.css"
import axios from "axios"
import { useState, useEffect } from "react"

const FeaturedProperties = () => {
    const [hotels, setHotels] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://backend-healing.herokuapp.com/hotels`)
            console.log(result)
            setHotels(result.data.data.map(item => {
                return {
                    id: item.id,
                    title: item.title,
                    image: item.image,
                    city: item.city,
                    addres: item.address,
                    desc: item.desc,
                    rating: item.rating,
                    featured: item.featured,
                    starttingPrice: item.starttingPrice
                }
            }))

        }
        fetchData()
    }, [])
    return (
        <div className="fp">
            {hotels.filter((hotel) => hotel.rating == 10).map((hotel) => (<div className="fpItem">
                <img src={hotel.image}
                    alt=""
                    className="fpImg"
                />
                <span className="fpName">{hotel.title}</span>
                <span className="fpCity">{hotel.city}</span>
                <span className="fpPrice">Starting From ${hotel.starttingPrice}</span>
                <div className="fpRating">
                    <button>{hotel.rating}</button>
                    <span>Excelent</span>
                </div>
            </div>))}
        </div>
    )
}

export default FeaturedProperties