import "./featured.css"

const Featured = () => {
    return (
        <div className="featured">
            <div className="featuredItem">
                <img src="https://cdn.pixabay.com/photo/2015/05/01/07/30/jakarta-747835_960_720.jpg"
                    alt="" className="featuredImg"
                />
                <div className="featuredTitles">
                    <h1>Jakarta</h1>
                </div>
            </div>
            <div className="featuredItem">
                <img src="https://cdn.pixabay.com/photo/2017/10/27/04/48/candi-2893245_960_720.jpg"
                    alt="" className="featuredImg"
                />
                <div className="featuredTitles">
                    <h1>Bandung</h1>

                </div>
            </div>
            <div className="featuredItem">
                <img src="https://images.unsplash.com/photo-1566176553949-872b2a73e04e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=385&q=80"
                    alt="" className="featuredImg"
                />
                <div className="featuredTitles">
                    <h1>Surabaya</h1>
                </div>
            </div>

        </div>
    )
}

export default Featured