import "./mailList.css"

const MailList = () => {
    return (
        <div className="mail">
            <span className="mailDesc">Subscribe & get updates on special promos</span>
            <div className="mailInputContainer">
                <input type="text" placeholder="Your Email" />
                <button>Subscribe</button>
            </div>
        </div>
    )
}

export default MailList